package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;
import edu.kit.informatik.dawn.ui.Command;

public class TestInputFormat {
    private Game game;

    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
        this.game = new Game();
    }

    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
    }
    

    @Test
    public void testState() {
        Terminal.writeInputBuffer("state");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 1;1:1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state a;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0;a");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("-"));
        
        Terminal.writeInputBuffer("State 0; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0 ;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0 ; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 0;0 ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state -1;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 1;-1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 11;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("state 1;16");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer(" state");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dasdstate");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("statesdas");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("statestate");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dfstatedfdsf");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    @Test
    public void testPrint() {
        Terminal.writeInputBuffer(" print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print 0;a");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print 0; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print 0 ;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print 0 ; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals(  "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------" + System.lineSeparator() 
                + "---------------"));
        
        Terminal.writeInputBuffer(" print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dasdprint");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printsdas");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printprint");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dfprintdfdsf");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }

    @Test
    public void testSetVc() {
        Terminal.writeInputBuffer("set-vc");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 1;1:1;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc a;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0;a");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("Set-vc 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0 ;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0 ; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 0;0 ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc -1;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 1;-1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 11;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vc 1;16");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer(" set-vc");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dasdset-vc");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vcsdas");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("set-vcset-vc");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dfset-vcdfdsf");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    private void prepareForRoll() {
        Terminal.writeInputBuffer("reset");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("set-vc 1;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    @Test
    public void testRoll() {
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer(" roll");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("dsdroll");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("rolldasda");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll dsad");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("Roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 2 ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 02");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
    }

    private void prepareForPlace() {
        prepareForRoll();
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
    }
    
    @Test
    public void testPlace() {
        Terminal.writeInputBuffer("place");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer(" place");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("dsdplace");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("placedasda");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place dsad");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place -1;0:-1;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0 ;0:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0; 0:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0 :0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0: 0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0:0 ;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0:0; 1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0:0;1 ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace(); 
        
        Terminal.writeInputBuffer("place 0;0:0;1:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place0;0:0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;00;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0;0;0;1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0:0:0:1");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();
        
        Terminal.writeInputBuffer("place 0001");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        prepareForPlace();   
    }
    
    @Test
    public void testQuit() {
        Terminal.writeInputBuffer(" quit");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit ");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit 0;a");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit 0;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit 0; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit 0 ;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit 0 ; 0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quit");
        Loop.loop(game);
        assertTrue(null == Terminal.peekOutputBuffer()); //no terminal output expected
        
        Terminal.writeInputBuffer(" quit");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dasdquit");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quitsdas");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("quitquit");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("dfquitdfdsf");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
}
