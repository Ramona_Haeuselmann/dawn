package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.board.Position;
import edu.kit.informatik.dawn.board.Token;
import edu.kit.informatik.dawn.board.TokenType;
import edu.kit.informatik.dawn.result.BreadthFirstSearch;

public class TestBFS {
    private Board board;
    private BreadthFirstSearch bfs;

    @Before
    public void setUp() throws Exception {
        this.board = new Board(11, 15);
        this.bfs = new BreadthFirstSearch();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testOnEdgeNoFree() {
        board.place(new Token(TokenType.VESTA, 1), new Position(0, 0), new Position(0, 0));
        board.place(new Token(TokenType.SONDE, 2), new Position(1, 0), new Position(1, 1));
        board.place(new Token(TokenType.SONDE, 2), new Position(0, 1), new Position(0, 1));
        
        assertTrue(bfs.count(board, new Position(0, 0)) == 0);
    }
    
    @Test
    public void testAllFree() {
        board.place(new Token(TokenType.VESTA, 1), new Position(0, 0), new Position(0, 0));
        
        assertTrue(bfs.count(board, new Position(0, 0)) == 164);
    }
    
    @Test
    public void testMixed1() {
        board.place(new Token(TokenType.VESTA, 1), new Position(0, 0), new Position(0, 0));
        
        board.place(new Token(TokenType.SONDE, 2), new Position(0, 2), new Position(0, 3));
        board.place(new Token(TokenType.SONDE, 2), new Position(1, 1), new Position(1, 2));
        board.place(new Token(TokenType.SONDE, 1), new Position(1, 4), new Position(1, 4));
        board.place(new Token(TokenType.SONDE, 3), new Position(2, 5), new Position(4, 5));
        board.place(new Token(TokenType.SONDE, 2), new Position(5, 3), new Position(5, 4));
        board.place(new Token(TokenType.SONDE, 2), new Position(3, 3), new Position(3, 4));
        board.place(new Token(TokenType.SONDE, 3), new Position(3, 0), new Position(3, 2));
        
        //System.out.println(board.toString());
        
        assertTrue(bfs.count(board, new Position(0, 0)) == 8);
    }
    
    @Test
    public void testMixed2() {
        board.place(new Token(TokenType.VESTA, 1), new Position(2, 4), new Position(2, 4));
        
        board.place(new Token(TokenType.SONDE, 2), new Position(0, 2), new Position(0, 3));
        board.place(new Token(TokenType.SONDE, 2), new Position(1, 1), new Position(1, 2));
        board.place(new Token(TokenType.SONDE, 1), new Position(1, 4), new Position(1, 4));
        board.place(new Token(TokenType.SONDE, 3), new Position(2, 5), new Position(4, 5));
        board.place(new Token(TokenType.SONDE, 2), new Position(5, 3), new Position(5, 4));
        board.place(new Token(TokenType.SONDE, 2), new Position(3, 3), new Position(4, 3));
        board.place(new Token(TokenType.SONDE, 3), new Position(3, 0), new Position(3, 2));
        
        //System.out.println(board.toString());
        
        assertTrue(bfs.count(board, new Position(2, 4)) == 10);
    }


}
