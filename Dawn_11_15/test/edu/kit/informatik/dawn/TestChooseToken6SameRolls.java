package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

@RunWith(Parameterized.class)
public class TestChooseToken6SameRolls {
Game game;
    
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { 2, new int[] {2,3,4,5,6,7}},
                 { 3, new int[] {3,2,4,5,6,7}}, 
                 { 4, new int[] {4,3,5,2,6,7}}, 
                 { 5, new int[] {5,4,6,3,7,2}}, 
                 { 6, new int[] {6,5,7,4,3,2}}, 
                 { 7, new int[] {7,6,5,4,3,2}}  
           });
    }
    
    @Parameter(0)
    public int repeatedRoll;
    
    @Parameter(1)
    public int[] placeOrder;
    

    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = false;
    }

    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
        Terminal.printToConsoleInTestMode = false;
    }
    
    @Test
    public void test() {
        //6x same roll
        //checks if order is successfull
        String sRoll;
        if (repeatedRoll == 7) {
            sRoll = "DAWN";
        } else {
            sRoll = Integer.toString(repeatedRoll);
        }
        
        Terminal.writeInputBuffer("set-vc 10;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;1:0;" + placeOrder[0]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;13");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 1;1:1;" + placeOrder[1]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;1:2;" + placeOrder[2]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;13");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 3;1:3;" + placeOrder[3]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 4;1:4;" + placeOrder[4]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;13");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        
        
        Terminal.writeInputBuffer("roll " + sRoll);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;1:5;" + placeOrder[5]);
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 10;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
    }

}
