package edu.kit.informatik.dawn;


import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FileTest {

    public static final String OUTPUT_TOO_SMALL = "Ausgabe ist zu klein.";
    public static final String OUTPUT_TOO_BIG = "Ausgabe ist zu groß.";

    @Parameters
    public static Collection<String> pathsToTestFiles() {
        List<String> paths = new ArrayList<>();
        final File directory = new File("./test/res/");

        listFileEntries(paths, directory);

        return paths;
    }

    private static void listFileEntries(List<String> list, final File directory) {
        for (final File entry : directory.listFiles()) {
            if (entry.isDirectory()) {
                listFileEntries(list, entry);
            } else {
                list.add(entry.getPath());
            }
        }
    }

    @Parameter
    public String filePath;

    @Test
    public void testFromFile() {
        Terminal.testMode = true;
        Game game = new Game();
//        GameControl gc = new GameControl();
//        TestUI ui = new TestUI(gc);
        System.out.println("\nNow Testing: " + filePath + "\n");
        try {
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            String line = file.readLine();
            while (line != null) {
                String[] lineElements = line.split("#");
                System.out.println(lineElements[0]);
                //List<String> output = ui.simulateInput(lineElements[0]);
                Terminal.writeInputBuffer(lineElements[0]);
                Loop.loop(game);
               
                List<String> output = new ArrayList<>();
                try {
                    String result = Terminal.readOutputBuffer();
                    output = Arrays.asList(result.split(System.lineSeparator()));
                } catch (NoSuchElementException e){
                    //e.printStackTrace();
                }
                
                
                int expectedOutputSize = lineElements.length - 2;
                assertTrue(OUTPUT_TOO_SMALL, output.size() >= expectedOutputSize);
                assertTrue(OUTPUT_TOO_BIG, output.size() <= expectedOutputSize);
                for (int i = 0; i < expectedOutputSize; i++) {
                    String compare = lineElements[1 + i];
                    if (compare.charAt(0) == '^') {
                        compare = compare.substring(1);
                        assertTrue(lineElements[lineElements.length - 1], output.get(i).startsWith(compare));
                    } else {
                        assertTrue(lineElements[lineElements.length - 1], output.get(i).equals(compare));
                    }
                }
                line = file.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            game.reset();
        } catch (DawnGameInputException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //gc.reset();
    }

}
