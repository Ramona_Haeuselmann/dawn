package edu.kit.informatik.dawn;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.board.Game;

public class TestPlaceNotPossible {
    Game game;
    
    @Before
    public void setUp() throws Exception {
        game = new Game();
        Terminal.testMode = true;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test1() {
        //Testet wenn das Spielfeld so verbaut ist, dass beim W�rfeln einer 6 diese nicht platziert werden kann
        //(4 ist auch noch �brig)
        //Dabei wird auch getestet wenn Vesta im letzten Place der Phase eingebaut wird, dass es dann mit set-vc weiter geht
        //und wenn Ceres inmitten der Phase eingebaut wird, dass es dann mit roll weiter geht
        
        Terminal.writeInputBuffer("set-vc 4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;5:1;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;5:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;5:4;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 3;6:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;6:3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;7:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 6;5:10;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;7:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;7:5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 5;6:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;0:5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Vesta stuck
        Terminal.writeInputBuffer("move 3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        //conitnue with set Ceres
        Terminal.writeInputBuffer("set-vc 4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;9:1;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;9:4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;9:4;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 3;8:4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;8:3;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 5;8:4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;8:5;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Ceres stuck
        Terminal.writeInputBuffer("move 3;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;10");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 5;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        //continue with roll
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 6;9:10;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Ceres stuck -> continue with roll
        Terminal.writeInputBuffer("roll 6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //can't be placed -> check board state as expected
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("-----+++++-----" + System.lineSeparator()
                                                    + "-----+++++-----" + System.lineSeparator() 
                                                    + "-----+++++-----" + System.lineSeparator() 
                                                    + "-----+++++-----" + System.lineSeparator() 
                                                    + "-----+V+C+-----" + System.lineSeparator() 
                                                    + "+++++++++++++++" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----"));
        
        
    }

    @Test
    public void test2() {
        //kleine Variation von test1
        //Testet wenn das Spielfeld so verbaut ist, dass beim W�rfeln einer 6 diese nicht platziert werden kann
        //(4 ist auch noch �brig)
        //Dabei wird auch getestet wenn Vesta im letzten Place der Phase eingebaut wird, dass es dann mit set-vc weiter geht
        //und wenn Ceres inmitten der Phase eingebaut wird, dass es dann mit roll weiter geht
        
        Terminal.writeInputBuffer("set-vc 4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;5:1;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;5:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;5:4;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 3;6:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 4");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;6:3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;7:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 6;5:10;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;7:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 0;7:5;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 5;6:4;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;0:5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Vesta stuck
        Terminal.writeInputBuffer("move 3;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 5;6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        //------------------------------------------------------------------------
        
        //conitnue with set Ceres
        Terminal.writeInputBuffer("set-vc 4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 2");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;8:3;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 4;9:4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll 3");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 2;9:4;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("move 5;8:4;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("roll DAWN");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 5;8:5;14");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Ceres stuck
        Terminal.writeInputBuffer("move 3;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;10");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 5;8");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("move 4;7");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        //continue with roll
        Terminal.writeInputBuffer("roll 5");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("place 6;9:10;9");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //Ceres stuck -> continue with roll
        Terminal.writeInputBuffer("roll 6");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //6 cant be placed (versuche 4, ist auch noch �brig)
        Terminal.writeInputBuffer("place 7;0:10;0");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        //can't be placed -> check board state as expected
        Terminal.writeInputBuffer("print");
        Loop.loop(game);
        assertTrue(Terminal.readOutputBuffer().equals("-----+++-------" + System.lineSeparator()
                                                    + "-----+++-------" + System.lineSeparator() 
                                                    + "-----+++++-----" + System.lineSeparator() 
                                                    + "-----+++++-----" + System.lineSeparator() 
                                                    + "-----+V+C+-----" + System.lineSeparator() 
                                                    + "+++++++++++++++" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----" + System.lineSeparator() 
                                                    + "-----+---+-----"));
        
        
    }

}
