/**
 * 
 */
package edu.kit.informatik.dawn.board;

import java.util.ArrayList;
import java.util.List;
import edu.kit.informatik.dawn.DawnGameInputException;
import edu.kit.informatik.dawn.StateMachine.GameState;
import edu.kit.informatik.dawn.rules.DiceSymbol;
import edu.kit.informatik.dawn.rules.Role;
import edu.kit.informatik.dawn.rules.Rules;
import edu.kit.informatik.dawn.rules.RulesDawn1115;
import edu.kit.informatik.dawn.ui.Command;
import edu.kit.informatik.dawn.ui.MessageStrings;

/**
 * Represents the Dawn Game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Game {
    /**
     * Number of rows used for the playing field
     */
    public static final int ROW_COUNT = 11;
    
    /**
     * Number of columns used for the playing field
     */
    public static final int COL_COUNT = 15;
    
    
    private Board board; //playing field
    private Rules rules; //rules for this game
    private GameState state; //state of the game to control game
    private Token currentMCToken; //last placed token of mission control
    private DiceSymbol rolled; //roll result
    
    /**
     * Constructor that initializes a new Game object
     */
    public Game() {
        init();
    }
    
    /**
     * Initializes the Game object
     */
    private void init() {
        this.board = new Board(ROW_COUNT, COL_COUNT);
        this.rules = new RulesDawn1115();
        this.state = new GameState();
        Role.resetAll();
    }
    
    /*
     * Checks if the given command is allowed in the current game state
     * Throws InputException if not
     */
    private void checkCommandAllowed(Command cmd) throws DawnGameInputException {
        if (!rules.isCommandAllowed(this.state, cmd)) {
            throw new DawnGameInputException(MessageStrings.CMD_NOT_ALLOWED.toString());
        }
    }
    
    /**
     * Returns the current state of the playing field at the given position
     * 
     * @param pos position in playing field to get the state from
     * @return state of the field as string
     * @throws DawnGameInputException if command is not allowed in current game state
     */
    public String getCellState(Position pos) throws DawnGameInputException {   
        checkCommandAllowed(Command.STATE);
        
        Cell c = board.getCell(pos);
        if (c == null) {
            throw new IllegalArgumentException("position out of range.");
        } else {
            return c.toString();
        }     
    }
    
    /**
     * Returns the playing field as string representation for visualization
     * 
     * @return playing field as string
     * @throws DawnGameInputException if command is not allowed in current game state
     */
    public String printBoard() throws DawnGameInputException {
        checkCommandAllowed(Command.PRINT);
        
        return this.board.toString();
    }
    
    /**
     * Places a token of player Nature on playing field
     * @param pos position to place token
     * @throws DawnGameInputException if command is not allowed in current game state 
     * or placing at given position not possible
     */
    public void setVc(Position pos) throws DawnGameInputException {
        checkCommandAllowed(Command.SET_VC);
        
        //get correct token from player (Vesta or Ceres), depending on phase
        List<Token> toPlace = Role.NATURE.getPossibleTokens(this.state.getPhase(), 1);
        
        assert toPlace != null;
        
        //try to place, if unsuccessful exception is thrown in place
        this.rules.place(this.board, toPlace, pos, pos); 
        //if place was successful switch to next game state
        this.state.next();
    }
    
    /**
     * Lets the player Nature roll with the Dawn Dice
     * 
     * @param rolled roll result
     * @throws DawnGameInputException if command is not allowed in current game state 
     */
    public void roll(DiceSymbol rolled) throws DawnGameInputException {
        checkCommandAllowed(Command.ROLL);
        
        this.rolled = rolled;
        this.state.next();
    }
    
    /**
     * Places a token of player Mission Control at given Position
     * 
     * @param start start position of token
     * @param end end position of token
     * @throws DawnGameInputException if command is not allowed in current game state 
     * or placing at given position not possible
     */
    public void place(Position start, Position end) throws DawnGameInputException {
        checkCommandAllowed(Command.PLACE);
        
        //get possible tokens to place depending on roll result and available tokens
        List<Token> possibleTokens = Role.MISSION_CONTROL
                .getPossibleTokens(state.getPhase(), rolled.getValue());

        assert possibleTokens != null;
        
        //tries to place, if not successful exception is thrown in place
        this.currentMCToken = this.rules.place(this.board, possibleTokens, start, end); 
        assert this.currentMCToken != null;
        this.state.next();
        
        //is luminary (Vesta or Ceres) now stuck?
        //get luminary
        Token luminary = Role.NATURE.getTokenSet(this.state.getPhase()).get(1); 
        if (board.isLuminaryStuckHV(luminary.getType())) {
            //if luminary can't be moved any more switch to nest game state
            //move action gets skipped
            this.state.next();
        }
    }
    
    /**
     * Moves current Nature token on given path (steps) if possible
     * 
     * @param steps path to move on
     * @throws DawnGameInputException if command is not allowed in current game state 
     * or move not possible
     */
    public void move(List<Position> steps) throws DawnGameInputException {
        checkCommandAllowed(Command.MOVE);
        
        //get token to move depending on phase (Vesta or Ceres)
        Token toMove = Role.NATURE.getTokenSet(this.state.getPhase()).get(1);
        //get current position of token to move
        Position pos = this.board.getLuminaryPosition(toMove.getType());

        assert pos != null;
        
        //at start position to the beginning of List for validation of path
        List<Position> allSteps = new ArrayList<>(steps);
        allSteps.add(0, pos);
        
        //tries to move, if not successful exception is thrown in place        
        this.rules.move(this.board, allSteps, this.currentMCToken.getLength());
        //if move was successful switch to next game state
        this.state.next();
    }
    
    /**
     * Calculates the game result at the end of the game
     * 
     * @return result of the game
     * @throws DawnGameInputException if command is not allowed in current game state 
     */
    public int getResult() throws DawnGameInputException {
        checkCommandAllowed(Command.SHOW_RESULT);
        
        return this.rules.calcResult(this.board);
    }
    
    /**
     * Lets a player reset the current gave to start over
     * 
     * @throws DawnGameInputException if command is not allowed in current game state 
     */
    public void reset() throws DawnGameInputException {
        checkCommandAllowed(Command.RESET);
        
        init();
    }
}
