/**
 * 
 */
package edu.kit.informatik.dawn.board;

/**
 * Represents a token for dawn game
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Token {
    private TokenType type; //type of token
    private int length; //length of token
    private boolean movable; //is this token movable due to the rules?
    private boolean used; //token available or not
    
    /**
     * Constructor to create a new token
     * 
     * @param type token type
     * @param length length of token (width is implicitly 1)
     */
    public Token(TokenType type, int length) {
        this.type = type;
        this.length = length;
        this.used = false;
        //set used property depending on type
        this.movable = this.type.isLuminary();
    }
    
    /**
     * Gets the type of this token
     * 
     * @return type of the token
     */
    public TokenType getType() {
        return this.type;
    }
    
    /**
     * Use this token
     */
    public void use() {
        assert !used;
        this.used = true;
    }
    
    /**
     * Checks if this token is already used
     * 
     * @return true if used, false if not
     */
    public boolean isUsed() {
        return this.used;
    }
    
    /**
     * Gets length of this token
     * 
     * @return length of this token
     */
    public int getLength() {
        return this.length;
    }
    
    /**
     * Checks if this token can be moved on the playing field after placing
     * 
     * @return true if movable, false if not
     */
    public boolean isMovable() {
        return this.movable;
    }
    
    @Override
    public String toString() {
        return this.type.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + length;
        result = prime * result + (movable ? 1231 : 1237);
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Token other = (Token) obj;
        if (length != other.length)
            return false;
        if (movable != other.movable)
            return false;
        if (type != other.type)
            return false;
        return true;
    }
}
