/**
 * 
 */
package edu.kit.informatik.dawn.board;

/**
 * Represents a type of token
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum TokenType {
    /**
     * Represents type Vesta
     */
    VESTA("V", true),
    
    /**
     * Represents type Ceres
     */
    CERES("C", true),
    
    /**
     * Represents type sonde
     */
    SONDE("+", false);
    
    private final String symbol; //string representation
    private final boolean luminary; 
    
    /*
     * Private constructor to init type with string representation and isLuminary
     */
    private TokenType(String symbol, boolean luminary) {
        this.symbol = symbol;
        this.luminary = luminary;
    }
    
    /**
     * Gets if this type is a luminary type
     * 
     * @return true if is luminary, false if not
     */
    public boolean isLuminary() {
        return this.luminary;
    }
    
    @Override
    public String toString() {
        return this.symbol;
    }

}
