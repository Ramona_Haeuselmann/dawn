/**
 * 
 */
package edu.kit.informatik.dawn.ui;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.dawn.DawnGameInputException;
import edu.kit.informatik.dawn.board.Game;

/**
 * Entry point for Dawn game.
 * Handles user interaction
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class CommandHandler {

    /**
     * Handles user interaction for Dawn Game
     * 
     * @param args all parameters passed will be ignored
     */
    public static void main(String[] args) {
        Game game = new Game(); //game object to perform actions on
        boolean quit = false; //signals if user entered quit command
        Command command = null; //input command
        
        do {
            try {
                //try to match user input to a command
                command = Command.matchAndExecute(Terminal.readLine(), game);
                if (command.isQuit()) {
                    quit = true; //exit program if user inputs quit command
                }
            } catch (DawnGameInputException e) {
                Terminal.printError(e.getMessage()); //print error messages to the user
            }
        } while (command == null || !quit); 
    } 
}
