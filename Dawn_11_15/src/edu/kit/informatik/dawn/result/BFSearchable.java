/**
 * 
 */
package edu.kit.informatik.dawn.result;

/**
 * Interface for an object to be searchable via breadth first search (BFS) algorithm
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public interface BFSearchable {
    /**
     * Visits the object during BFS algorithm.
     */
    void visit();
    
    /**
     * Checks if object was already visited by BFS algorithm
     * 
     * @return true if object was already visited, false if not
     */
    boolean isVisited();
    
    /**
     * Resets the object to not visited.
     * Use after BFS algorithm so that object can be visited again in a new run of the BFS algorithm.
     */
    void resetVisit();
}
