/**
 * 
 */
package edu.kit.informatik.dawn.result;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.board.Cell;
import edu.kit.informatik.dawn.board.Position;

/**
 * Implementation of free connected cell counter with modified breadth first search algorithm
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class BreadthFirstSearch implements FreeConnectedCellCounter {

    @Override
    public int count(Board board, Position root) {
        Queue<BFSearchable> queue = new LinkedList<>();
        BFSearchable item;
        List<Position> neighbors;
        int resultCounter = 0;
        Cell c;
        
         //add start cell to queue
        queue.add(board.getCell(root));
        
        //bfs algorithm
        while (!queue.isEmpty()) {
            item = queue.remove(); //get first element in queue
            neighbors = ((Cell) item).getPosition().getOrthogonalPositions(); //get surrounding cell positions
            //check surrounding cell positions
            //if ok (on board, not occupied, not already visited) add to queue
            for (Position pos : neighbors) {
                c = board.getCell(pos);
                if (c != null) { //valid cell on board?
                    if (!c.isVisited() && !c.isOccupied()) { //add to queue
                        c.visit();
                        queue.add(c);
                        resultCounter++;
                    }
                }
            }
        }
        
        //reset visit
        for (int row = 0; row < board.getRowCount(); row++) {
            for (int col = 0; col < board.getColumnCount(); col++) {
                board.getCell(new Position(row, col)).resetVisit();
            }
        }
        
        return resultCounter;
    }

}
